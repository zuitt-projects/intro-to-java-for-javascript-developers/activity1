package com.example.activity1;

import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {
        Scanner appName = new Scanner(System.in);

        System.out.print("Enter First Name: ");
        String firstName = appName.nextLine();

        System.out.print("Enter Last Name: ");
        String lastName = appName.nextLine();

        System.out.print("Enter English Grade: ");
        int gradeEnglish = appName.nextInt();

        System.out.print("Enter Mathematics Grade: ");
        int gradeMathematics = appName.nextInt();

        System.out.print("Enter Science Grade: ");
        int gradeScience = appName.nextInt();

        int average = (gradeEnglish + gradeMathematics + gradeScience) / 3;

        System.out.println("My complete name is " + firstName + " " + lastName + ". " + "My average grade of the 3 subjects is " + average + ".");
    }
}
